import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, switchMap, map, startWith } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { JaguarState, Status } from 'src/app/modules/jaguar/state/jaguar.state';
import { Store } from '@ngxs/store';
import { JaguarUpdateStatusAction } from 'src/app/modules/jaguar/state/jaguar.actions';
import { interval, Subject, Observable } from 'rxjs';

export enum HttpMethod{
  get = "GET",
  post = "POST",
  put = "PUT",
}

@Injectable({
  providedIn: 'root'
})
export class VoyageService {

  status$: Subject<Status>;

  constructor(
    private httpClient: HttpClient,
    private afAuth: AngularFireAuth,
    private store: Store
  ) {
    interval(5000).pipe(
      startWith(0),
      switchMap(_=>{
          return this.getDoorStatus();
        }
      )
    ).subscribe(newStatus=>this.store.dispatch(new JaguarUpdateStatusAction(newStatus)))
  }

  public setDoor(status: Status){
    if(![Status.open, Status.closed].includes(status)){
      throw(`Invalid status: ${status}`);
    }

    let wantOpen = 0;
    if(status === Status.open){
      wantOpen = 1;
    }
    
    return this.makeCall('/api/door/open', HttpMethod.put, `wantOpen: ${wantOpen}`).pipe(
      map((results:any) => {

        let newStatus = Status.unknown;

        if(results.isOpen === "1"){
          newStatus = Status.open
        }

        if(results.isOpen === "0"){
          newStatus = Status.closed
        }

        this.store.dispatch(new JaguarUpdateStatusAction(newStatus))
      })
    );
  }

  public getDoorStatus(){
    return this.makeCall('/api/door/open').pipe(
      map(
        (result)=>{
          let newStatus = Status.unknown;
          if(result == "0"){
            newStatus = Status.closed;
          }
          if(result == "1"){
            newStatus = Status.open;
          }
          return newStatus;
        }
      )
    );
  }

  private makeCall( path: string, method: HttpMethod = HttpMethod.get, options: string = ""){

    let user = this.afAuth.auth.currentUser;

    console.log(user.metadata);

    if(!user){
      console.warn("User Not Authorized");
      return null;
      ;
    }

    let token = JSON.parse(JSON.stringify(user)).stsTokenManager.accessToken;

    let headers = new HttpHeaders({
      //'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET',
      'Content-Type': 'plain/text',
      'Authorization': token
    });

    if(method === HttpMethod.get){
        return this.httpClient.get(path, {headers: headers});
    }

    if(method === HttpMethod.post){
      return this.httpClient.post(path, options, {headers: headers});
    }

    if(method === HttpMethod.put){
      return this.httpClient.put(path, options, {headers: headers});
    }
   
  }
}
