import { TestBed } from '@angular/core/testing';

import { VoyageService } from './voyage.service';

import {
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment.prod';
import { NgxsModule } from '@ngxs/store';
import { JaguarState } from 'src/app/modules/jaguar/state/jaguar.state';

describe('VoyageService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      NgxsModule.forRoot([JaguarState]),
      AngularFireModule.initializeApp(environment.firebase),
      AngularFireAuthModule,
      AngularFirestoreModule,
    ]
  }));

  it('should be created', () => {
    const service: VoyageService = TestBed.get(VoyageService);
    expect(service).toBeTruthy();
  });
});
