import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { User } from '../model/user';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) { }

  getUsers(){
    return this.afs.collection('users', ref => ref.orderBy('displayName') ).valueChanges().pipe(
      map(users => {
        let returnUsers = users;

        users.map(user => {

          let returnUser = user;

          if(!user.hasOwnProperty('roles')){
            returnUser['roles'] = {
              admin: false, 
              trusted: false
            }
          }

          if(!user['roles'].hasOwnProperty('admin')){
            user['roles'].admin = false;
          };
          if(!user['roles'].hasOwnProperty('trusted')){
            user['roles'].trusted = false;
          };

          return returnUser;
        });

        return returnUsers;

      })
    );
  }

  setUserTrust(user: User, trust: boolean){
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const data = {
      roles: {
        trusted: trust,
      }
    }

    return userRef.set(data, { merge: true })
  }
}
