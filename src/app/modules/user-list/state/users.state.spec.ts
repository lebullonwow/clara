import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { UsersState, UsersStateModel } from './users.state';

describe('Users state', () => {
    let store: Store;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([UsersState])]
        }).compileComponents();
        store = TestBed.get(Store);
    }));

    it('should create an empty state', () => {
        const actual = store.selectSnapshot(UsersState.getState);
        const expected: UsersStateModel = {
            items: []
        };
        expect(actual).toEqual(expected);
    });

});
