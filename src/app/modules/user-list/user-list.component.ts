import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { switchMap, map, startWith } from 'rxjs/operators';
import { User, Roles, UserAdapter } from 'src/app/core/model/user';
import { Observable, Subject } from 'rxjs';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { UserService } from 'src/app/core/user/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public users$: Observable<any>;

  constructor(
    public authService: AuthService,
    public afs: AngularFirestore,
    public userService: UserService,
  ) { 


    let firestoreRef = AngularFirestoreDocument;

    this.users$ = this.userService.getUsers();
    //const users: AngularFirestoreDocument<any> = this.afs.doc(`users/`).get();
  }

  ngOnInit() {
    
  }

  setUserTrust(user: User, trust: boolean){
    this.userService.setUserTrust(user, trust);
  }

  userHasRole(role: string, user: User){
    console.log(role);
    return user.hasOwnProperty('roles')
      && user.roles.hasOwnProperty(role)
      && user.roles[role] === true;
  }

}
