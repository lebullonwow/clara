import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JaguarComponent } from './jaguar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment.prod';
import { VoyageService } from 'src/app/core/voyage/voyage.service';
import { Subject } from 'rxjs';

describe('JaguarComponent', () => {
  let component: JaguarComponent;
  let fixture: ComponentFixture<JaguarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AngularFireModule.initializeApp(environment.firebase), 
        AngularFirestoreModule,
        AngularFireAuthModule,
        RouterTestingModule,
        
      ],
      providers: [
        VoyageService,
      ],
      declarations: [ JaguarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JaguarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //TODO: :c
  // it('should create', () => {
  //   let voyagerService = component.voyageService;
  //   let mySpy = spyOn(voyagerService, 'getValue').and.returnValue(new Subject<string>())
  //   expect(mySpy).toHaveBeenCalled()
  //   expect(component).toBeTruthy();
  // });
});
