import { Component, OnInit } from '@angular/core';
import { VoyageService } from 'src/app/core/voyage/voyage.service';
import { Store, Select, Selector } from '@ngxs/store';
import { Status, JaguarState, JaguarStateModel } from './state/jaguar.state';
import { Observable } from 'rxjs';
import { JaguarUpdateStatusAction } from './state/jaguar.actions';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-jaguar',
  templateUrl: './jaguar.component.html',
  styleUrls: ['./jaguar.component.scss']
})
export class JaguarComponent implements OnInit {

  //@Select(state => state.animals) animals$: Observable<string[]>;

  //@Select(UserState) user$: Observable<UserStateModel>;
  @Select(JaguarState) status$: Observable<Status>;
  public statusText: String = "";
  public status: Status = Status.unknown;

  constructor(
    public voyageService: VoyageService,
    public store: Store,
    private _snackBar: MatSnackBar,
  ) { 
    //I'm sure I can use pipes instead of this
    this.status$.subscribe((val: any)=>{
      this.statusText = val.statusText;
      this.status = val.status;
    })
  }

  ngOnInit() {}

  toggleDoor(){
    if(this.status === Status.closed){
      this.voyageService.setDoor(Status.open).subscribe(
        result => this._snackBar.open("Opening the door", null, {
          duration: 3000
        })
      )
    }
    if(this.status === Status.open){
      this.voyageService.setDoor(Status.closed).subscribe(
        result => this._snackBar.open("Closing the door", null, {
          duration: 3000
        })
      )
    }
    if(!([Status.open, Status.closed].includes(this.status))){
       this._snackBar.open("Unable to send a request", null, {
        duration: 3000
      })
    }
  }

}
