import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { JaguarState, Status } from './jaguar.state';
import { JaguarUpdateStatusAction } from './jaguar.actions';

describe('Jaguar actions', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([JaguarState])]
    }).compileComponents();
    store = TestBed.get(Store);
  }));

  it('should compile', () => {
    expect(store).toBeTruthy();
  });
  // Without Unit Testing support from NGXS, this seems to be difficult versus the time I have
  // it('should create an action and add an item', () => {
  //   store.dispatch(new JaguarUpdateStatusAction(Status.unknown));
  //   store.select(state => state.jaguar.items).subscribe((items: string[]) => {
  //     expect(items).toEqual(jasmine.objectContaining([ 'item-1' ]));
  //   });
  // });

});
