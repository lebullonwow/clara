import { Status } from "./jaguar.state";

export class JaguarUpdateStatusAction {
  static readonly type = '[Jaguar] UpdateState';
  constructor(public status: Status) { }
}

