import { State, Action, StateContext, Selector } from '@ngxs/store';
import { JaguarUpdateStatusAction } from './jaguar.actions';

export enum Status{
  open = "Open",
  moving = "Moving",
  closed = "Closed",
  unknown = "Unknown",
}

export class JaguarStateModel {
  public status: Status;
  public statusText: string;
}

@State<JaguarStateModel>({
  name: 'jaguar',
  defaults: {
    status: Status.unknown,
    statusText: "Unknown (Initial)"
  }
})

export class JaguarState {

  @Action(JaguarUpdateStatusAction)
  update(ctx: StateContext<JaguarStateModel>, action: JaguarUpdateStatusAction) {
    if(ctx.getState().status != action.status){

      let status = action.status;
      let statusText = "Unknown";

      //debugger;

      //do something with user state
      if(status.toString() === Status.open){
        statusText = "Open";
      }
      if(status.toString() === Status.closed){
        statusText = "Closed";
      }
      if(status.toString() === Status.moving){
        statusText = "Moving"
      }

      ctx.patchState({
        status: action.status,
        statusText: statusText,
      });
    }
    // const state = ctx.getState();
    // ctx.setState({ items: [ ...state.items, action.payload ] });
  }
}
